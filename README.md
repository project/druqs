# Druqs (Drupal Quick Search)

Druqs provides a quick search field for content, users, UUIDs, taxonomies and
menus. The field is by default placed in the admin toolbar, but also ships with
a block you can use instead.

[Project page](https://www.drupal.org/project/druqs)

### Installation

Please follow the steps:

1. `composer require 'drupal/druqs:2.0.x-dev@dev'` or add the modules directory
manually.
2. Enable the module at `/admin/modules`.
3. Manage configuration profiles at `/admin/config/user-interface/druqs`.
4. If you're not using the admin toolbar, add the Druqs block at
`/admin/structure/block`.

### Add your own results

Other modules can subscribe to the Druqs search event to add their own results.
Please extend the event subscriber `DruqsSubscriber` and consult respective
subscribers in `\druqs\EventSubscriber` for examples. For backwards
compatibility, we also provide the hook `hook_druqs_search()` - see
`druqs.api.php` for more info.

### Themes

The quick search is intended to be used in the admin toolbar. But, you can place
a block inside your site structure. We can not guarantee that the block will
integrate well into your selected theme - but we are looking to improve the
theming soon. The block was tested in the themes Claro, Seven and Bartik.

### Soon

We are planning the following changes:

* Drupal 10 compatibility
* Better styling integration for different themes
* Search custom entities
* Search speed improvements
* jQuery deprecation
* Tests

### Maintainers

* Mario Kaiser ([ehmprah](https://www.drupal.org/u/ehmprah))
* Simon Bäse ([simonbaese](https://www.drupal.org/u/simonbaese))
