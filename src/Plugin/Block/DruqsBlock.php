<?php

namespace Drupal\druqs\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides the Druqs search field as a block.
 *
 * @Block(
 *   id = "druqs",
 *   admin_label = @Translation("Druqs"),
 *   category = @Translation("Administration"),
 * )
 */
class DruqsBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      'tab' => [
        '#type' => 'search',
        '#attributes' => [
          'id' => 'druqs-input',
          'placeholder' => $this->t('Quick search'),
        ],
        '#suffix' => '<div id="druqs-results"></div>',
      ],
      '#wrapper_attributes' => [
        'class' => ['druqs-tab'],
      ],
      '#attached' => [
        'library' => ['druqs/drupal.druqs'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access druqs');
  }

}
