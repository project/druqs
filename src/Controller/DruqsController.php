<?php

namespace Drupal\druqs\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\druqs\Event\DruqsSearchEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Utility\UrlHelper;

/**
 * Provides route responses for Druqs.
 */
class DruqsController extends ControllerBase {

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Constructs a DruqsController object.
   *
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   */
  final public function __construct(EventDispatcherInterface $event_dispatcher) {
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('event_dispatcher'));
  }

  /**
   * Returns search results as JSON for an input provided by a query parameter.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The output with search results.
   */
  public function search(Request $request) {

    // Get the druqs configuration.
    $config = $this->config('druqs.configuration');

    // Get the search query from POST data and create a new search event.
    $input = trim($request->get('query'));
    $search = new DruqsSearchEvent($input, $config);

    // Dispatch search event and get search results.
    $search = $this->eventDispatcher->dispatch($search);
    $output = $search->getResults();

    // Provide search hook for BC purposes. One should use an event subscriber.
    // @todo Deprecate the use of druqs_search hook.
    // Build args for the hook invocation.
    $args = [
      'input' => $input,
      'results_current' => $search->resultsCount(),
      'results_per_source' => $config->get('results_per_source'),
      'results_max' => $config->get('results_max'),
    ];

    // Invoke hook_druqs_search() to allow modules to add their results.
    if ($results = $this->moduleHandler()->invokeAll('druqs_search', [&$args])) {
      foreach ($results as $result) {
        // Format the actions.
        $actions = [];
        foreach ($result['actions'] as $title => $uri) {
          $actions[$title] = UrlHelper::stripDangerousProtocols($uri);
        }
        // Add formatted output.
        $output[] = [
          'type' => $result['type'],
          'title' => $result['title'],
          'actions' => $actions,
        ];
      }
    }

    return new JsonResponse($output);
  }

}
