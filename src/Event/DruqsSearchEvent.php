<?php

namespace Drupal\druqs\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Config\Config;

/**
 * Defines a Druqs search event.
 */
class DruqsSearchEvent extends Event {

  /**
   * Input provided by user.
   *
   * @var string
   */
  protected $input;

  /**
   * The search sources.
   *
   * @var string[]
   */
  protected $sources;

  /**
   * The search results.
   *
   * @var array
   */
  protected $results;

  /**
   * Maximum count of search results.
   *
   * @var int
   */
  protected $resultsMaximum;

  /**
   * Count of search results per source.
   *
   * @var int
   */
  protected $resultsPerSource;

  /**
   * Constructs a DruqsSearchEvent object.
   *
   * @param string $input
   *   The search term.
   * @param \Drupal\Core\Config\Config $config
   *   The druqs configuration.
   */
  public function __construct(string $input, Config $config) {
    $this->input = $input;
    $this->sources = $config->get('search_sources');
    $this->resultsMaximum = $config->get('results_max');
    $this->resultsPerSource = $config->get('results_per_source');
    $this->results = [];
  }

  /**
   * Returns the search term.
   *
   * @return string
   *   The input.
   */
  public function getInput() {
    return $this->input;
  }

  /**
   * Sets the input.
   *
   * Usually, the input would be set with the constructor. Use this method,
   * e.g., to change the input in an EventSubscriber.
   *
   * @param string $input
   *   The input.
   */
  public function setInput(string $input) {
    $this->input = $input;
    return $this;
  }

  /**
   * Returns the search sources.
   *
   * @return string[]
   *   The search sources.
   */
  public function getSources() {
    return $this->sources;
  }

  /**
   * Returns the current limit for search results.
   *
   * The current limit depends on the current results count, the maximum allowed
   * search results and allowed search results per source.
   *
   * @return int
   *   The current limit.
   */
  public function currentLimit() {
    return min(
      $this->resultsPerSource,
      $this->resultsMaximum - $this->resultsCount()
    );
  }

  /**
   * Returns the current count of search results.
   *
   * @return int
   *   The results count.
   */
  public function resultsCount() {
    return count($this->results);
  }

  /**
   * Appends results to the search.
   *
   * @param array $results
   *   The search results to append.
   */
  public function appendResults(array $results) {
    $this->results = array_merge($this->results, $results);
  }

  /**
   * Returns the search results.
   *
   * @return array
   *   The search results.
   */
  public function getResults() {
    return $this->results;
  }

  /**
   * Sets the search results.
   *
   * In the standard case, use the appendResults() method. With this method one
   * could, e.g., overwrite the search results with an EventSubscriber and stop
   * the event propagation.
   *
   * @param array $results
   *   The search results to set.
   */
  public function setResults(array $results) {
    $this->results = $results;
  }

}
