<?php

namespace Drupal\druqs\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\druqs\Event\DruqsSearchEvent;

/**
 * Defines the Druqs node subscriber.
 */
class DruqsNodeSubscriber extends DruqsSubscriber {

  /**
   * The node storage.
   *
   * @var \Drupal\node\NodeStorageInterface
   */
  protected $nodeStorage;

  /**
   * Constructs a DruqsNodeSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->nodeStorage = $entity_type_manager->getStorage('node');
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function lookUp(DruqsSearchEvent $search) {

    // Kill event subscriber, when not searching for nodes.
    if (!in_array('node', $search->getSources())) {
      return;
    }

    // Load nodes by title.
    $node_ids = $this->nodeStorage->getQuery()
      ->accessCheck(TRUE)
      ->condition('title', $search->getInput(), 'CONTAINS')
      ->range(0, $search->currentLimit())->execute();
    $nodes = $this->nodeStorage->loadMultiple($node_ids);

    // Format results.
    $results = [];
    /** @var \Drupal\node\NodeInterface $node */
    foreach ($nodes as $node) {
      $results[] = [
        'type' => 'Content (' . $node->getType() . ')',
        'title' => $node->getTitle(),
        'actions' => [
          'view' => $node->toUrl('canonical')->toString(),
          'edit' => $node->toUrl('edit-form')->toString(),
        ],
      ];
    }

    $search->appendResults($results);
  }

}
