<?php

namespace Drupal\druqs\EventSubscriber;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Uuid\Uuid;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityMalformedException;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\druqs\Event\DruqsSearchEvent;

/**
 * Defines the Druqs uuid subscriber.
 */
class DruqsUuidSubscriber extends DruqsSubscriber {

  /**
   * The priority of the event subscriber.
   *
   * This event subscriber should fire early, because the search result is
   * guaranteed to be unique.
   */
  const PRIORITY = 100;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Constructs a DruqsUuidSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function lookUp(DruqsSearchEvent $search) {

    // Only search with this subscriber, if the input is valid uuid.
    if (!Uuid::isValid($search->getInput())) {
      return;
    }

    // Get content entity types and prioritize sources from config.
    $entity_types = $this->getContentEntityTypes();
    $entity_types = $this->prioritizeEntityTypesBySources($entity_types, $search);

    // Query all content entity types by uuid.
    foreach ($entity_types as $entity_type) {
      try {
        $id = $this->entityTypeManager->getStorage($entity_type->id())
          ->getQuery()
          ->accessCheck(TRUE)
          ->condition('uuid', $search->getInput())
          ->execute();
        if (!empty($id)) {
          $id = reset($id);
          break;
        }
      }
      catch (PluginNotFoundException | InvalidPluginDefinitionException $e) {
        return;
      }
    }

    // If no ID was found, return early.
    if (!isset($id) || !isset($entity_type)) {
      return;
    }

    // Attempt to load the entity.
    try {
      /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
      $entity = $this->entityTypeManager->getStorage($entity_type->id())
        ->load($id);
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
      return;
    }

    $search->appendResults([
      [
        'type' => $entity_type->getLabel(),
        'title' => $this->getTitleFromEntity($entity, $entity_type),
        'actions' => [
          'view' => $this->getUrlFromEntity($entity, $entity_type, 'canonical'),
          'edit' => $this->getUrlFromEntity($entity, $entity_type, 'edit-form'),
        ],
      ],
    ]);

    // The search result is unique.
    // @todo Address deprecation, when dropping Drupal 8 support.
    //   https://www.drupal.org/node/3159012
    $search->stopPropagation();
  }

  /**
   * Returns all content entity types with Uuid key.
   *
   * @return array
   *   Content entity types with uuid key.
   */
  protected function getContentEntityTypes() {
    $entity_types = [];
    foreach ($this->entityTypeManager->getDefinitions() as $definition) {
      if ($definition instanceof ContentEntityType &&
        $definition->hasKey('uuid')) {
        $entity_types[] = $definition;
      }
    }
    return $entity_types;
  }

  /**
   * Returns all content entity types prioritized by sources.
   *
   * @param array $entity_types
   *   The entity types.
   * @param \Drupal\druqs\Event\DruqsSearchEvent $search
   *   The search event.
   *
   * @return array
   *   The entity types prioritized by sources.
   */
  protected function prioritizeEntityTypesBySources(
    array $entity_types,
    DruqsSearchEvent $search
  ) {
    $entity_types_ids = array_map(function ($c) {
      return $c->id();
    }, $entity_types);
    foreach ($search->getSources() as $source) {
      if ($key = array_search($source, $entity_types_ids)) {
        array_unshift($entity_types, $entity_types[$key]);
        unset($entity_types[$key + 1]);
        $entity_types = array_values($entity_types);
      }
    }
    return $entity_types;
  }

  /**
   * Attempts to get the url for a template from the entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The respective entity type.
   * @param string $template
   *   The requested url template.
   *
   * @return string
   *   The canonical url.
   */
  protected function getUrlFromEntity(
    EntityInterface $entity,
    EntityTypeInterface $entity_type,
    string $template
  ) {
    if ($entity_type->hasLinkTemplate($template)) {
      try {
        return $entity->toUrl($template)->toString();
      }
      catch (EntityMalformedException $e) {
      }
    }
    return '';
  }

  /**
   * Attempts to fetch a title from the entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The respective entity type.
   *
   * @return string
   *   The predicted title.
   */
  protected function getTitleFromEntity(
    ContentEntityInterface $entity,
    EntityTypeInterface $entity_type
  ) {
    $title = 'No title found';
    $fields = $this->entityFieldManager
      ->getBaseFieldDefinitions($entity_type->id());
    if (array_key_exists('title', $fields)) {
      $title = $entity->get('title')->value;
    }
    elseif (array_key_exists('name', $fields)) {
      $title = $entity->get('name')->value;
    }
    return $title;
  }

}
