<?php

namespace Drupal\druqs\EventSubscriber;

use Drupal\druqs\Event\DruqsSearchEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Defines an abstract Druqs subscriber.
 */
abstract class DruqsSubscriber implements EventSubscriberInterface {

  /*
   * Use to prioritize search subscribers by overwriting the constant in the
   * extending class.
   */
  const PRIORITY = 0;

  /**
   * Handles a druqs search event.
   *
   * @param \Drupal\druqs\Event\DruqsSearchEvent $search
   *   The search event.
   */
  abstract protected function lookUp(DruqsSearchEvent $search);

  /**
   * {@inheritdoc}
   */
  final public static function getSubscribedEvents() {
    return [DruqsSearchEvent::class => ['lookUp', static::PRIORITY]];
  }

}
