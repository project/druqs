<?php

namespace Drupal\druqs\EventSubscriber;

use Drupal\Core\Menu\MenuLinkTreeInterface;
use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\druqs\Event\DruqsSearchEvent;

/**
 * Defines the Druqs menu handler.
 */
class DruqsMenuSubscriber extends DruqsSubscriber {

  /**
   * The menu tree.
   *
   * @var \Drupal\Core\Menu\MenuLinkTreeInterface
   */
  protected $menuTree;

  /**
   * Constructs a DruqsMenuHandler object.
   *
   * @param \Drupal\Core\Menu\MenuLinkTreeInterface $menu_tree
   *   The menu tree.
   */
  public function __construct(MenuLinkTreeInterface $menu_tree) {
    $this->menuTree = $menu_tree;
  }

  /**
   * {@inheritdoc}
   */
  public function lookUp(DruqsSearchEvent $search) {

    // Iterate all menus in search sources.
    $menu_sources = array_filter($search->getSources(), function ($s) {
      return substr($s, 0, 5) == 'menu_';
    });
    foreach ($menu_sources as $menu_source) {

      // Load the menu tree without any params. Then, transform the tree. Only
      // show links that are accessible and flatten the tree for easier search.
      $menu_name = substr($menu_source, 5);
      $tree = $this->menuTree->load($menu_name, new MenuTreeParameters());
      $manipulators = [
        ['callable' => 'menu.default_tree_manipulators:checkAccess'],
        ['callable' => 'menu.default_tree_manipulators:flatten'],
      ];
      $tree = $this->menuTree->transform($tree, $manipulators);

      // Find menu items by title and format the result.
      $results = [];
      foreach ($tree as $item) {
        if (stripos($item->link->getTitle(), $search->getInput()) !== FALSE) {
          $results[] = [
            'type' => 'Menu (' . $menu_name . ')',
            'title' => $item->link->getTitle(),
            'actions' => [
              'goto' => $item->link->getUrlObject()->toString(),
            ],
          ];
        }
      }

      $search->appendResults($results);
    }

  }

}
