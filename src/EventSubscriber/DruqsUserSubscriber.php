<?php

namespace Drupal\druqs\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\druqs\Event\DruqsSearchEvent;

/**
 * Defines the Druqs user subscriber.
 */
class DruqsUserSubscriber extends DruqsSubscriber {

  /**
   * The user storage.
   *
   * @var \Drupal\user\UserStorageInterface
   */
  protected $userStorage;

  /**
   * Constructs a DruqsUserSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->userStorage = $entity_type_manager->getStorage('user');
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function lookUp(DruqsSearchEvent $search) {

    // Kill event subscriber, when not searching for users.
    if (!in_array('user', $search->getSources())) {
      return;
    }

    // Load users by name.
    $user_ids = $this->userStorage->getQuery()
      ->accessCheck(TRUE)
      ->condition('name', $search->getInput(), 'CONTAINS')
      ->condition('uid', 0, '!=')
      ->range(0, $search->currentLimit())->execute();
    $users = $this->userStorage->loadMultiple($user_ids);

    // Format the results.
    $results = [];
    /** @var \Drupal\user\UserInterface $user */
    foreach ($users as $user) {
      $results[] = [
        'type' => 'User',
        'title' => $user->get('name')->value,
        'actions' => [
          'view' => $user->toUrl('canonical')->toString(),
          'edit' => $user->toUrl('edit-form')->toString(),
        ],
      ];
    }

    $search->appendResults($results);
  }

}
