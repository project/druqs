<?php

namespace Drupal\druqs\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\druqs\Event\DruqsSearchEvent;

/**
 * Defines the Druqs taxonomy subscriber.
 */
class DruqsTaxonomySubscriber extends DruqsSubscriber {

  /**
   * The taxonomy term storage.
   *
   * @var \Drupal\taxonomy\TermStorageInterface
   */
  protected $termStorage;

  /**
   * Constructs a DruqsTaxonomySubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->termStorage = $entity_type_manager
      ->getStorage('taxonomy_term');
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function lookUp(DruqsSearchEvent $search) {

    // Kill event subscriber, when not searching for taxonomy terms.
    if (!in_array('taxonomy', $search->getSources())) {
      return;
    }

    // Load taxonomy terms by name.
    $term_ids = $this->termStorage->getQuery()
      ->accessCheck(TRUE)
      ->condition('name', $search->getInput(), 'CONTAINS')
      ->range(0, $search->currentLimit())->execute();
    $terms = $this->termStorage->loadMultiple($term_ids);

    // Format the results.
    $results = [];
    /** @var \Drupal\taxonomy\TermInterface $term */
    foreach ($terms as $term) {
      $results[] = [
        'type' => 'Taxonomy (' . $term->bundle() . ')',
        'title' => $term->getName(),
        'actions' => [
          'view' => $term->toUrl('canonical')->toString(),
          'edit' => $term->toUrl('edit-form')->toString(),
        ],
      ];
    }

    $search->appendResults($results);
  }

}
